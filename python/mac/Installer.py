#!/usr/bin/env python3
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton,QLineEdit,QFormLayout,QLabel,QGridLayout,QProgressBar,QVBoxLayout,QHBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot, QObject, pyqtSignal
from modules.uninstall import rm_dirs as rmdirs, array as u_array
from modules.dotfiles import install_dotfiles as dot_install
from modules.install import array as i_array, codium_packages as cpac
import pexpect, os, subprocess, shutil, time, sys
from subprocess import PIPE, Popen
from modules.expect    import listener as listen
from threading import *

def Install(passwrd):
    for cmd in i_array:
        ex.setUpdatesEnabled(False)
        ex.update_status(cmd)
        ex.setUpdatesEnabled(True)
        listen(cmd,passwrd)

    dot_install()

    for pac in cpac:
        ex.setUpdatesEnabled(False)
        ex.update_status('codium --install {}'.format(pac))
        ex.setUpdatesEnabled(True)
        #ex.update_progress(cmd)
        subprocess.run('/usr/local/bin/codium --install-extension {} --force'.format(pac), shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)#stdout=open(hello-mac.log, 'a'), stderr=open(hello-mac.err, 'a'))
#sys.stdout=open('hello-install.log', 'a'), sys.stdout=open('hello-install.err', 'a'))
        #with open(os.devnull, 'wb') as devnull:
        #    try:
                #subprocess.check_call(['codium --install-extension {} --force'.format(pac)], stdout=devnull, stderr=subprocess.STDOUT)
            #except:
                #continue

    subprocess.run('/bin/zsh &', shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)#stdout=open(hello-mac.log, 'a'), stderr=open(hello-mac.err, 'a'))stdout=open(hello-mac.log, 'a'), stderr=open(hello-mac.err, 'a'))

def Uninstall(passwrd):
    rmdirs()
    for cmd in u_array:
        ex.setUpdatesEnabled(False)
        ex.update_status(cmd)
        ex.setUpdatesEnabled(True)
        listen(cmd,passwrd)

def Main(passwrd):

    # if shutil.which("brew"):
    #     Uninstall(passwrd)
    # else:
    #     ex.setUpdatesEnabled(False)
    #     ex.init_status(cmd)
    #     ex.setUpdatesEnabled(True)

    Install(passwrd)
    ex.setUpdatesEnabled(False)
    ex.update_status("Installation Complete! You May Close This Window.")
    ex.setUpdatesEnabled(True)
    #ex.repaint()

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.pbar_status = 0
        self.title = 'Hello Installer!'
        self.left = 10
        self.top = 10
        self.width = 400
        self.height = 150
        self.setMaximumWidth(400)

        self.passwordlabel = QLabel("Password: ")
        self.current_command = QLabel("", self)
        self.current_command.setMaximumWidth(350)
        self.current_command.setText("Welcome! Enter Your Password To Continue.")

        self.pbar=QProgressBar(self)
        self.pbar.setMaximum(70)
        self.pbar.setFormat("%p")
        #self.pbar.setGeometry(30, 40, 450, 75)

        self.password = QLineEdit()
        self.password.setEchoMode(QLineEdit.Password)
        self.password.setStyleSheet('lineedit-password-character: 9679')
        self.password.setPlaceholderText("Enter Password Here")

        self.installbutton = QPushButton("Install")

        outer_layout = QVBoxLayout()
        input_layout = QHBoxLayout()
        input_layout.addWidget(self.passwordlabel)
        input_layout.addWidget(self.password)
        input_layout.addWidget(self.installbutton)
        outer_layout.addLayout(input_layout)

        status_layout = QGridLayout()
        status_layout.addWidget(self.current_command,0,0)
        status_layout.addWidget(self.pbar,1,0)
        outer_layout.addLayout(status_layout)

        self.setLayout(outer_layout)

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.password.returnPressed.connect(self.on_click)

        self.installbutton.clicked.connect(self.on_click)

        self.show()

    def update_status(self,cmd):
        self.current_command.setText(str(cmd))
        self.current_command.update()
        self.pbar_status+=1
        self.pbar.setValue(self.pbar_status)


    def init_status(self):
        self.pbar_status+=8
        self.pbar.setValue(self.pbar_status)

    def thread(self):
        t1=Thread(target=self.main_program)
        t1.start()

    def main_program(self):
        Main(self.password)

    def error_output (self,cmd):
        self.current_command.setText(str(cmd))
        self.current_command.update()

    @pyqtSlot()
    def on_click(self):

        if(True):
            try:
                subprocess.run('echo "Begin Install"', shell=True, stdout=open('hello-mac.log', 'w'))
                open('hello-mac.err', 'w').close()
                self.installbutton.setEnabled(False)
                self.password.setReadOnly(True)
                self.password.setDisabled(True)
                self.thread()
            except:
                self.error_output("Error: Install Failed.  See error.log for details.")

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
