#!/usr/bin/env python3
import os
from pathlib import Path
import subprocess

def install_dotfiles():

    # value = str('source ~/.hello-zsh')
    # with open(str(Path.home())+ '/.zshrc', 'a+') as f:
    #    f.close()

    with open(str(Path.home()) + "/.zshrc", "w") as my_zshrc:
        my_zshrc.write(zshrc)
        my_zshrc.close()

    with open(str(Path.home()) + "/.profile", "w") as my_profile:
        my_profile.write(profile)
        my_profile.close()

    with open(str(Path.home()) + "/.config/starship.toml", "w") as my_starship:
        my_starship.write(starship)
        my_starship.close()

    with open(str(Path.home()) + "/.ssh/config", "w") as my_ssh_config:
        my_ssh_config.write(ssh_config)
        my_ssh_config.close()

    with open(str(Path.home()) + "/Library/Application Support/VSCodium/User/settings.json", "w") as my_codium:
        my_codium.write(codium_settings)
        my_codium.close()

ssh_config="""Host gitlab
	Preferredauthentications publickey
	Hostname gitlab.com
	User git
	IdentityFile ~/.ssh/gitlab

Host gitlab.com
	Preferredauthentications publickey
	Hostname gitlab.com
	User git
	IdentityFile ~/.ssh/gitlab
"""

zshrc="""#!/usr/bin/env zsh

osascript -e 'tell application "Terminal" to set the font name of window 1 to "Hack Nerd Font Mono"',
source ~/.hello-profile

### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

### End of Zinit's installer chunk
zinit wait lucid nocd depth=1 \
      atinit='ZSH_BASH_COMPLETIONS_FALLBACK_LAZYLOAD_DISABLE=true' \
      for 3v1n0/zsh-bash-completions-fallback

zinit light-mode for \
    hlissner/zsh-autopair \
    arzzen/calc.plugin.zsh \
    zpm-zsh/colorize \
    zdharma-continuum/fast-syntax-highlighting \
    zsh-users/zsh-history-substring-search \
    zsh-users/zsh-autosuggestions \
    zdharma-continuum/history-search-multi-word \
    wookayin/fzf-fasd

MODE_CURSOR_VIINS="#657b83 steady bar"

KEYTIMEOUT=1

RPROMPT=""

WORDCHARS='*?[]~=&;!#$%^(){}<>:\./\'
HISTFILE=~/.zsh_history
HISTSIZE=5000
SAVEHIST=5000

setopt append_history
setopt autocd
setopt braceccl
setopt combining_chars
setopt correct
setopt hist_expire_dups_first
setopt hist_find_no_dups
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt hist_verify
setopt inc_append_history
setopt interactive_comments
setopt multios
setopt nobeep
setopt nocaseglob
setopt nocheckjobs
setopt nohup
setopt numericglobsort

bindkey "^[OH" beginning-of-line
bindkey "^[[H" beginning-of-line
bindkey -a 'H' beginning-of-line

bindkey "^[OF"  end-of-line
bindkey "^[[F"  end-of-line

bindkey '^[[1;5C' forward-word
bindkey '^[[C' forward-word

bindkey '^[[1;5D' backward-word
bindkey '^[[D' backward-word

bindkey '^[[C' forward-char
bindkey '^[[D' backward-char

bindkey '^?' backward-delete-char

bindkey "^[[3~" delete-char
bindkey "^[3;5~" delete-char

bindkey '^[[3;5~' delete-word

bindkey '^H' backward-kill-word

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

autoload -U compinit && compinit

cd "$HOME/Documents/repos" > /dev/null 2>&1

function chpwd() {
    emulate -L zsh
    ls
}

if [ -e "/usr/bin/exa" ]; then
	clear
	ls
fi

if [ "$(command -v starship)" ]; then
    eval "$(starship init zsh)"
fi
"""

starship= """# Don't print a new line at the start of the prompt
add_newline = false
command_timeout = 5000

# Replace the "❯" symbol in the prompt with "➜"
[character]                            # The name of the module we are configuring is "character"
success_symbol = "[>>](#859900 bold)"     # The "success_symbol" segment is being set to "➜" with the color "bold green"
vicmd_symbol = "[>>](#859900 bold)"     # The "success_symbol" segment is being set to "➜" with the color "bold green"
error_symbol = "[>>](#dc322f bold)"     # The "success_symbol" segment is being set to "➜" with the color "bold green"
# Disable the package module, hiding it from the prompt completely
[package]
disabled = true

[username]
style_user = "#6c71c4 bold"
style_root = "#dc322f bold"
format = "[$user]($style) @ "
disabled = false
show_always = true

[hostname]
ssh_only = false
format =  "[$hostname](#6c71c4 bold) "
disabled = false

[env_var]
variable = "ICON_IP_ADDRESS"
format = "$env_value "

[battery]
disabled = true

[git_status]
format = '([\[$all_status$ahead_heind\]]($style) )'
style = "bold green"


[perl]
format="🐪  "
"""

profile= """#!/usr/bin/env zsh

# update PATH (where executables live)
 PATH="$PATH:$HOME/bin:$HOME/.cargo/bin:$HOME/.local/bin:/opt/homebrew/bin/:/usr/local/bin"
 PATH="$PATH:/usr/local/lib/python3.10/site_packages:/Users/$USER/Library/Python/3.9/bin"
 export HOMEBREW_NO_ANALYTICS=1
 export HOMEBREW_NO_GOOGLE_ANALYTICS=1
# Allow sudo + aliases
alias sudo='sudo '

# Settings for interactive zsh
if [ -n "$PS1" ] && [ "$ZSH_NAME" ]; then

	# colorize man pages
	export LESS_TERMCAP_mb=$'\e[1;32m'
	export LESS_TERMCAP_md=$'\e[1;32m'
	export LESS_TERMCAP_me=$'\e[0m'
	export LESS_TERMCAP_se=$'\e[0m'
	export LESS_TERMCAP_so=$'\e[01;33m'
	export LESS_TERMCAP_ue=$'\e[0m'
	export LESS_TERMCAP_us=$'\e[1;4;31m'

	# Colorize grep and diff
	alias grep="grep --color=auto"
	alias diff="diff --color=auto"

    alias pip="/usr/local/bin/pip3"
    alias python="/usr/local/bin/python3"

	IP_ADDRESS="$(ipconfig getifaddr en0)"
	export ICON_IP_ADDRESS="ﲾ $IP_ADDRESS"
	# pretty bat
	if [ "$(command -v bat)" ]; then
		alias bat="bat --wrap=character --force-colorization --paging always -p"
	fi

	# Alias ls to exa
	if [ "$(command -v exa)" ]; then
		export EXA_COLORS='ln=36'
		alias ls="exa -F --color=always --group-directories-first --icons"
		alias la='ls -a --git-ignore'
		alias ll='ls -l --git-ignore'
		alias lla='ls -la --git-ignore'
		alias lt='ls -T -L 2 --git-ignore'
		alias ld='lt -D'
	fi
fi
"""

codium_settings="""
{
    "terminal.integrated.fontFamily": "Hack Nerd Font",
    "terminal.integrated.fontSize": 15,
    "editor.fontFamily": "Hack Nerd Font",
    "editor.fontLigatures": true,
    "workbench.startupEditor": "none",
    "security.workspace.trust.startupPrompt": "never"
}

"""
