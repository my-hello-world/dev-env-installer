#!/usr/bin/env python3
import os
from pathlib import Path
import subprocess

array = [
    "/usr/bin/pip3 uninstall -y pycodestyle jedi pipreqs > /dev/null 2>&1",
    "/usr/local/bin/pip3 uninstall -y pycodestyle jedi pipreqs > /dev/null 2>&1",
    "npm uninstall eslint css-lint remark-lint > /dev/null 2>&1",
    "brew uninstall git node unzip vscodium",
    "brew uninstall git tldr man-db entr cmake starship bat exa",
    "brew uninstall ripgrep fd wget imagemagick",
    "sudo rm -rf /usr/local/bin/codium",
    "/bin/bash -c \"$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/uninstall.sh)\""]

rm_array=[str(Path.home()) + "/.zinit",
          str(Path.home()) + "/.hello-zshrc",
          str(Path.home()) + "/.helloprofile",
          str(Path.home()) + "/Documents/repos/github",
          str(Path.home()) + "/.vscode-oss/*",
          str(Path.home()) + "/.config/starship.toml",
          str(Path.home()) + "/Library/Application\ Support/VSCodium/User/settings.json"]

def rm_dirs():
    for idx in rm_array:
        subprocess.run(['rm -rf {}'.format(idx)], shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)#stdout=open(hello-mac.log, 'a'), stderr=open(hello-mac.err, 'a')) )
