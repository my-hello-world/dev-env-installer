
function WSL-Install {

    if( -not ( Get-Command "choco" -ErrorAction SilentlyContinue )) {
        Set-ExecutionPolicy Bypass -Scope Process -Force;
        [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
    }

    choco feature enable -n=allowGlobalConfirmation

    # Install chocolatey packages
    choco install -y --nocolor -r python nodejs shellcheck microsoft-windows-terminal git vscodium cascadia-code-nerd-font nerd-fonts-3270 nerdfont-hack cmake make mingw

    python.exe -m pip install --upgrade pip

    # python jedi
    pip install  jedi pycodestyle
    npm install csslint eslint remark

    # Install WSL
    wsl --install Ubuntu-22.04 --no-launch
    wsl.exe --install Ubuntu-22.04 --no-launch
}

function WSL-Uninstall {

    pip uninstall -y jedi pycodestyle

    npm uninstall csslint eslint remark
    wsl --unregister Ubuntu-22.04
    wsl --unregister Ubuntu
}

function Uninstall-Chocolatey {
        rm \ProgramData\Chocolatey
}

function Uninstall-ChocoPackages {

        choco feature enable -n=allowGlobalConfirmation
        choco uninstall -y --nocolor -r python nodejs shellcheck microsoft-windows-terminal git vscodium cascadia-code-nerd-font wsl-ubuntu-2004 nodejs.install
}

WSL-InstallPrompt
