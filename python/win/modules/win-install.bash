#!/bin/bash
set_term_default(){

    my_uid="$(date +%s)"
    cp "$settings_path/settings.json" "$settings_path/settings.json-$my_uid"

    recent="$(/bin/ls settings_path/setings.json-* | tail -1)"

    while read line; do
        if [[ "$line" == *"guid"* ]]; then
            guid=$line
        fi

        if [[ "$line" == *"Ubuntu 22.04.1"* ]]; then
            break
        fi
    done < "$recent"

    font_string="\"font\": \n {\n \"face\": \"CaskaydiaCove Nerd Font Mono\"\n },"
#"commandline": "C:\\Windows\\System32\\wsl.exe -d hello-arch -u root",

    my_number=`echo "$guid" | sed -E "s/.*\"\{(.*)\}\".*/\1/g"`

    sed -E "/.*$my_number.*/i $font_string" "$settings_path/settings.json" > "$settings_path/font-settings.json"

    sed -E "s/.*defaultProfile.*/     \"defaultProfile\": \"\{$my_number}\",/" "$recent" > "$settings_path/settings.json"
}

win_username="$(powershell.exe '$env:UserName')";
term_path="/mnt/c/Users/${win_username%?}/AppData/Local/Packages"
term_name=`/bin/ls "$term_path" | grep Terminal`
settings_path="$term_path/$term_name/LocalState"

 #heading "Updating Cache and Packages"

 sudo apt update -y && sudo apt upgrade -y

 sudo apt install -y python3-pip python3 npm unzip zsh fontconfig \
     shellcheck tldr man-db entr cmake fontconfig \
     imagemagick python3-pip bat exa ripgrep fd-find dos2unix

 curl -sS https://starship.rs/install.sh | sh

 mkdir -p "$HOME/.local/bin"

 python3 -m pip install --upgrade setuptools
 python3 -m pip install --upgrade pip

 # Install Linters
 npm install eslint css-lint remark-lint
 pip install pycodestyle jedi

 chsh -s /bin/zsh

# codium "C:\\Users\\${win_username%?}\\Documents\\repos\\hello-world" &

 zsh
