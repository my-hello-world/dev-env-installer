pub const PHRASES: [&'static str; 10] = [
    "Activating Chroniton Drive",
    "Navigating Asteroid Field",
    "Chasing Rogue Comet",
    "Observing Accretion Disk",
    "Refueling Anti-Matter Core",
    "Calculating Black Hole Density",
    "Studying Binary Star System",
    "Collecting Dark Matter Samples",
    "Launching Exoplanet Rover",
    "Preparing Landing Gear",
];
