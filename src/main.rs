#![allow(unused_must_use)]
mod install;
use install::*;

use iced::widget::{button, column, text};
use iced::widget::{Column, Image, ProgressBar};
use iced::{executor, time, window};
use iced::{Alignment, Application, Command, Element, Length, Settings, Subscription, Theme};

use std::path::Path;
use std::time::{Duration, Instant};

use simple_home_dir::*;
use std::fs::OpenOptions;

pub fn main() -> iced::Result {
    DevEnvInstaller::run(Settings::default())
}

#[derive(Debug, Clone)]
pub enum Message {
    Start,
    Wait(Instant),
    Continue(Instant),
    ExitInstaller,
}

#[derive(Debug, Clone)]
pub enum State {
    Idle,
    Waiting,
    Running { last_tick: Instant },
    Complete,
}

#[derive(Debug, Clone)]
pub struct DevEnvInstaller {
    duration: Duration,
    state: State,
    progress_phrases: Vec<&'static str>,
    checkpoint_idx: usize,
    total_time: f64,
}

impl Application for DevEnvInstaller {
    type Message = Message;
    type Theme = Theme;
    type Executor = executor::Default;
    type Flags = ();

    fn new(_flags: ()) -> (DevEnvInstaller, Command<Message>) {
        (
            DevEnvInstaller {
                duration: Duration::default(),
                state: State::Idle,
                progress_phrases: PHRASES.to_vec(),
                checkpoint_idx: 0,
                total_time: 15.0e6, //microsec
            },
            Command::none(),
        )
    }

    fn theme(&self) -> Self::Theme {
        Theme::Dark
    }

    fn title(&self) -> String {
        format!("Dev Env Installer").to_string()
    }

    fn view(&self) -> Element<Message> {
        fn container(title: &str) -> Column<Message> {
            column![text(title).size(50)]
        }
        let rocket_image = container("")
            .push(Image::new("images/rocket.png"))
            .width(Length::Fill)
            .height(Length::Fill)
            .padding(20)
            .align_items(Alignment::Center);

        match self.state {
            State::Idle => {
                let launch_button = container("").push(button("Launch").on_press(Message::Start));

                container("Blast Off Into Programing!")
                    .push(rocket_image)
                    .push(launch_button)
                    .padding(20)
                    .align_items(Alignment::Center)
                    .into()
            }

            State::Running { .. } | State::Waiting => {
                let progress_phrase = container("")
                    .push(
                        text(format!(
                            "{}",
                            self.progress_phrases[self.checkpoint_idx as usize]
                        ))
                        .size(40),
                    )
                    .align_items(Alignment::Center);

                let mut mu_sec = self.duration.as_micros() as f64;
                let mu_subsec = self.duration.subsec_millis();

                let progress_bar = container("")
                    .push(
                        ProgressBar::new(0.0..=(self.total_time as f32), mu_sec as f32).width(500),
                    )
                    .align_items(Alignment::Center);

                let progress_duration = container("")
                    .push(
                        text(format!(
                            "{:0>2}.{:0>1}%",
                            (mu_sec * 100. / (self.total_time)).floor(),
                            mu_subsec / 100
                        ))
                        .size(40),
                    )
                    .align_items(Alignment::Center);

                let progress_content = container("")
                    .push(progress_phrase)
                    .push(progress_bar)
                    .push(progress_duration)
                    .padding(20)
                    .align_items(Alignment::Center);

                //let sec = self.duration.as_secs();
                // let e_count = (((sec % 3) + 3) % 3) as usize;
                // let ellipsis = ".".repeat(e_count).to_string();
                // let title = format!("Exploring The Galaxy{}", ellipsis).as_str();

                container("Exploring The Galaxy")
                    .push(rocket_image)
                    .push(progress_content)
                    .padding(20)
                    .align_items(Alignment::Center)
                    .into()
            }

            State::Complete => {
                let exit_button = container("")
                    .push(button("Land").on_press(Message::ExitInstaller))
                    .align_items(Alignment::Center)
                    .padding(10);

                container("Prepare For Landing!")
                    .push(rocket_image)
                    .push(exit_button)
                    .padding(20)
                    .align_items(Alignment::Center)
                    .into()
            }
        }
    }

    fn update(&mut self, msg: Message) -> Command<Message> {
        let checkpoint: String = format!(
            "{}/checkpoint",
            home_dir().unwrap().as_path().display().to_string()
        );

        match self.state {
            State::Waiting | State::Running { .. } => {
                if self.checkpoint_idx == (self.progress_phrases.len() - 1) {
                    ()
                } else if Path::new(&checkpoint).try_exists().unwrap() == false {
                    self.checkpoint_idx += 1;

                    OpenOptions::new()
                        .create(true)
                        .write(true)
                        .open(&checkpoint);
                    println!("");

                    self.state = State::Running {
                        last_tick: Instant::now(),
                    }
                }
            }
            _ => (),
        }

        if let State::Running { .. } = self.state {
            if let State::Waiting = self.state {}
        }

        match msg {
            Message::Start => {
                if self.duration.as_secs() < 1 {
                    OpenOptions::new().create(true).write(true).open(checkpoint);
                }

                self.state = State::Running {
                    last_tick: Instant::now(),
                }
            }

            Message::Continue(now) => {
                if let State::Running { last_tick } = &mut self.state {
                    self.duration += now - *last_tick;
                    *last_tick = now
                }
            }

            Message::Wait { .. } => {
                if (self.checkpoint_idx + 1) == self.progress_phrases.len() {
                    self.state = State::Complete
                } else {
                    Command::<Message>::none();
                }
            }
            Message::ExitInstaller => {
                return window::close();
            }
        }
        Command::none()
    }

    fn subscription(&self) -> Subscription<Message> {
        match self.state {
            State::Running { .. } => {
                let percent_complete = (self.duration.as_micros() as f64) / self.total_time;

                let percent_checkpoint =
                    (self.checkpoint_idx as f64 + 1.0) / (self.progress_phrases.len() as f64);

                if percent_complete >= (percent_checkpoint as f64) {
                    time::every(Duration::from_millis(50)).map(Message::Wait)
                } else {
                    time::every(Duration::from_millis(10)).map(Message::Continue)
                }
            }
            _ => Subscription::none(),
        }
    }
}
