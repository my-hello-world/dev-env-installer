Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

choco install -y --nocolor -r python microsoft-windows-terminal git vscodium pyenv

Invoke-WebRequest -UseBasicParsing -Uri "https://raw.githubusercontent.com/pyenv-win/pyenv-win/master/pyenv-win/install-pyenv-win.ps1" -OutFile "./install-pyenv-win.ps1"; &"./install-pyenv-win.ps1"
pyenv install 3.10.0
pyenv global 3.10.0

python3 -m pip install --upgrade setup tools
python3 -m pip install --upgrade pip

pip install jedi pycodestyle pyenv

codium --install-extension esbenp.prettier-vscode ms-python.python vscode-icons-team.vscode-icons

Invoke-WebRequest -Uri "https://gitlab.com/wuphysics87/code-to-the-stars/-/raw/main/scripts/settings.json?ref_type=heads" -OutFile "~\AppData\Roaming\VSCodium\User\settings.json"
