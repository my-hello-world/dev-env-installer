use iced::widget::Column;
use iced::widget::{self, button, column, text, text_input};
use iced::{executor, keyboard, subscription, theme};
use iced::{Application, Command, Element, Event, Settings, Subscription, Theme};

pub fn main() -> iced::Result {
    Example::run(Settings::default())
}

#[derive(Debug, Clone)]
enum Message {
    Submit,
    UpdateName(String),
    UpdateEmail(String),
    UpdatePassword(String),
    Event(Event),
}

struct Example {
    user: User,
}

pub struct User {
    name: String,
    email: String,
    password: String,
}

impl Application for Example {
    type Message = Message;
    type Theme = Theme;
    type Executor = executor::Default;
    type Flags = ();

    fn new(_flags: ()) -> (Example, Command<Message>) {
        (
            Example {
                user: User {
                    name: String::from(""),
                    email: String::from(""),
                    password: String::from(""),
                },
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("Image Toggler")
    }

    fn update(&mut self, msg: Message) -> Command<Message> {
        match msg {
            Message::UpdateName(name) => {
                self.user.name = name;
                Command::none()
            }
            Message::UpdateEmail(email) => {
                self.user.email = email;
                Command::none()
            }
            Message::UpdatePassword(password) => {
                self.user.password = password;
                Command::none()
            }
            Message::Submit => {
                println!("Name: {}", self.user.name);
                println!("Email: {}", self.user.email);
                println!("Password: {}", self.user.password);
                Command::none()
            }
            Message::Event(Event::Keyboard(keyboard::Event::KeyPressed {
                key_code: keyboard::KeyCode::Tab,
                modifiers,
            })) if modifiers.shift() => widget::focus_previous(),
            Message::Event(Event::Keyboard(keyboard::Event::KeyPressed {
                key_code: keyboard::KeyCode::Tab,
                ..
            })) => widget::focus_next(),
            Message::Event(_) => Command::none(),
        }
    }

    fn view(&self) -> Element<Message> {
        fn container(title: &str) -> Column<Message> {
            column![text(title).size(50)]
        }

        let name_box = text_input("Name", &self.user.name)
            .on_input(Message::UpdateName)
            .padding(10);
        let email_box = text_input("Email", &self.user.email)
            .on_input(Message::UpdateEmail)
            .padding(10);

        let pass_box = text_input("Password", &self.user.password)
            .on_input(Message::UpdatePassword)
            .password()
            .padding(10);

        let submit_button = if self.user.name.is_empty() == false
            && self.user.email.is_empty() == false
            && self.user.password.is_empty() == false
        {
            button("Submit").on_press(Message::Submit)
        } else {
            button("Submit").style(theme::Button::Secondary)
        };

        container("Enter Launch Codes")
            .push(name_box)
            .push(email_box)
            .push(pass_box)
            .push(submit_button)
            .into()
    }
    fn subscription(&self) -> Subscription<Self::Message> {
        subscription::events().map(Message::Event)
    }
}
