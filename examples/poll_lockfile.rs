use iced::time;
use iced::widget::{container, text};
use iced::{alignment, executor, window};
use iced::{Application, Color, Command, Element, Length, Settings, Subscription, Theme};
use simple_home_dir::*;
use std::fs;
use std::path::Path;
use std::time::{Duration, Instant};

pub fn main() -> iced::Result {
    Example::run(Settings::default())
}

#[derive(Debug, Clone)]
enum Message {
    GetUpdate(Instant),
}

struct Example {
    count: Vec<u32>,
}

impl Application for Example {
    type Message = Message;
    type Theme = Theme;
    type Executor = executor::Default;
    type Flags = ();

    fn new(_flags: ()) -> (Example, Command<Message>) {
        (
            Example {
                count: [1, 2, 3, 4, 5].to_vec(),
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("Image Toggler")
    }

    fn update(&mut self, msg: Message) -> Command<Message> {
        match msg {
            Message::GetUpdate { .. } => {
                let lock_file =
                    String::from(format!("{}/my_file.txt", home_dir().unwrap().display()));
                match Path::new(&lock_file.as_str()).exists() {
                    true => {
                        let _ = fs::remove_file(&lock_file);
                        if self.count.len() > 0 {
                            self.count.pop();
                        } else {
                            return window::close();
                        }
                    }
                    false => println!("no file"),
                };
            }
        };
        Command::none()
    }

    fn view(&self) -> Element<Message> {
        let title = text(self.count[self.count.len() - 1])
            .width(Length::Fill)
            .size(100)
            .style(Color::from([0.5, 0.5, 0.5]))
            .horizontal_alignment(alignment::Horizontal::Center);

        container(title).into()
    }

    fn subscription(&self) -> Subscription<Message> {
        time::every(Duration::from_millis(1000)).map(Message::GetUpdate)
    }
}
