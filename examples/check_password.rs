use iced::widget::Column;
use iced::widget::{self, button, column, text, text_input};
use iced::{executor, keyboard, subscription, theme};
use iced::{Application, Command, Element, Event, Settings, Subscription, Theme};
use rexpect::error::*;
use rexpect::spawn_bash;
use std::path::Path;
use std::thread;
use std::time::Duration;

pub fn check_sudo() -> Result<(), Error> {
    let mut p = spawn_bash(Some(150))?;
    p.send_line("sudo -S true < /dev/null &>/dev/null || touch /home/sean/sudo_fail")?;
    p.send_line("")?;
    Ok(())
}

pub fn check_password(password: &str) -> Result<(), Error> {
    let mut p = spawn_bash(Some(150))?;
    p.send_line("echo 'shit' | sudo tee -a  /home/sean/lockfile")?;
    p.send_line(&password)?;
    Ok(())
}

pub fn main() -> iced::Result {
    Example::run(Settings::default())
}

#[derive(Debug, Clone)]
enum Message {
    CheckSudo,
    Submit,
    UpdateName(String),
    UpdateEmail(String),
    UpdatePassword(String),
    Event(Event),
}

struct Example {
    user: User,
    state: State,
}

enum State {
    Idle,
    SudoFailure,
    EnterPassword,
    ErrorPassword,
    CorrectPassword,
}

pub struct User {
    name: String,
    email: String,
    password: String,
}

impl Application for Example {
    type Message = Message;
    type Theme = Theme;
    type Executor = executor::Default;
    type Flags = ();

    fn new(_flags: ()) -> (Example, Command<Message>) {
        (
            Example {
                user: User {
                    name: String::from(""),
                    email: String::from(""),
                    password: String::from(""),
                },
                state: State::Idle,
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("Image Toggler")
    }

    fn update(&mut self, msg: Message) -> Command<Message> {
        match msg {
            Message::CheckSudo => {
                check_sudo().ok();
                if Path::new("/home/sean/sudo_fail").exists() {
                    self.state = State::SudoFailure;
                } else {
                    self.state = State::EnterPassword;
                }
                Command::none()
            }
            Message::UpdateName(name) => {
                self.user.name = name;
                Command::none()
            }
            Message::UpdateEmail(email) => {
                self.user.email = email;
                Command::none()
            }
            Message::UpdatePassword(password) => {
                self.user.password = password;
                Command::none()
            }
            Message::Submit => {
                check_password(&self.user.password).ok();
                if Path::new("/home/sean/lockfile").exists() {
                    self.state = State::CorrectPassword;
                } else {
                    self.state = State::ErrorPassword;
                    thread::sleep(Duration::from_millis(1500));
                    self.state = State::EnterPassword;
                }
                Command::none()
            }
            Message::Event(Event::Keyboard(keyboard::Event::KeyPressed {
                key_code: keyboard::KeyCode::Tab,
                modifiers,
            })) if modifiers.shift() => widget::focus_previous(),
            Message::Event(Event::Keyboard(keyboard::Event::KeyPressed {
                key_code: keyboard::KeyCode::Tab,
                ..
            })) => widget::focus_next(),

            Message::Event(_) => Command::none(),
        }
    }

    fn view(&self) -> Element<Message> {
        fn container(title: &str) -> Column<Message> {
            column![text(title).size(50)]
        }

        match self.state {
            State::Idle => {
                let my_button = button("Start").on_press(Message::CheckSudo);
                container("Press Start").push(my_button).into()
            }

            State::SudoFailure => container("Launch Delayed :(").into(),

            State::EnterPassword | State::ErrorPassword => {
                let name_box = text_input("Name", &self.user.name)
                    .on_input(Message::UpdateName)
                    .padding(10);
                let email_box = text_input("Email", &self.user.email)
                    .on_input(Message::UpdateEmail)
                    .padding(10);

                let pass_box = text_input("Password", &self.user.password)
                    .on_input(Message::UpdatePassword)
                    .password()
                    .padding(10);

                let submit_button = if self.user.name.is_empty() == false
                    && self.user.email.is_empty() == false
                    && self.user.password.is_empty() == false
                {
                    button("Submit").on_press(Message::Submit)
                } else {
                    button("Submit").style(theme::Button::Secondary)
                };

                let heading: &str = if let State::ErrorPassword = self.state {
                    "INCORRECT LAUNCH CODE!"
                } else {
                    "Enter Launch Codes"
                };

                container(heading)
                    .push(name_box)
                    .push(email_box)
                    .push(pass_box)
                    .push(submit_button)
                    .into()
            }
            State::CorrectPassword => container("YAY PASSWORD STUFF").into(),
        }
    }
    fn subscription(&self) -> Subscription<Self::Message> {
        subscription::events().map(Message::Event)
    }
}
